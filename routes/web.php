<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $services=\App\Service::all();
    $quote=\App\Quote::where('status','Active')->first();
    return view('welcome',compact('services','quote'));
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile', 'HomeController@profile')->name('profile');
Route::post('/profile/update', 'HomeController@updateprofile')->name('updateprofile');
Route::post('/user/update/{id}', 'HomeController@updateuser')->name('updateuser');
Route::post('/role/update', 'HomeController@updateRole')->name('updateRole');
Route::get('/getdata', 'HomeController@getData')->name('getData');
Route::post('/user/result', 'UserController@searchuser')->name('admin.searchuser');
Route::post('/booking/result', 'ServiceController@searchbookings')->name('admin.searchbookings');

//User
Route::post('/user/register', 'ApiController@register')->name('register');
Route::get('/user/all', 'UserController@all')->name('admin.users');

//User
Route::get('/services/all', 'ServiceController@all')->name('admin.services');
Route::get('/admin/services/all', 'ServiceController@user_all')->name('user.services');
Route::post('/services/add', 'ServiceController@addservice')->name('admin.addservice');
Route::post('/services/update/{id}', 'ServiceController@updateservice')->name('admin.updateservice');
Route::post('/service/book', 'ServiceController@book')->name('book');
Route::post('/service/book/cancel', 'ServiceController@cancelbook')->name('cancelbook');
Route::get('/user/service/book', 'ServiceController@getUserBooking')->name('user.bookings');

//Bookings
Route::get('/user/service/reset', 'ServiceController@reset')->name('booking.reset');
Route::get('/user/service/bookings', 'ServiceController@bookings')->name('admin.booking');
Route::get('/admin/service/bookings/{id}', 'ServiceController@adminbooking')->name('admin.bookings');
Route::post('/user/service/adminbook', 'ServiceController@adminbook')->name('admin.book');
Route::get('/booking/view/{id}', 'ServiceController@view')->name('booking.view');


//Reports
Route::get('/report/service', 'ReportController@services')->name('report.service');
Route::get('/report/users', 'ReportController@users')->name('report.user');
Route::post('/report/service/data', 'ReportController@getservicedata')->name('report.getservicedata');
Route::post('/report/users/data', 'ReportController@getusersdata')->name('report.getusersdata');
Route::get('/report/booking/weekly', 'ReportController@byweek')->name('report.byweek');
Route::get('/report/booking/weekly/{week}', 'ReportController@byweekdata')->name('report.byweekdata');

//Quates
Route::get('/quotes', 'QuotesController@index')->name('admin.quotes');
Route::post('/quotes', 'QuotesController@addquote')->name('admin.addquote');
Route::post('/quotes/edit/{id}', 'QuotesController@editquote')->name('admin.editquote');
Route::get('/quotes/activate/{id}', 'QuotesController@activate')->name('admin.quote.activate');
