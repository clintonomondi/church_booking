@extends('layouts.common')

@section('content')

    <div class="page-error tile">
        <div class="row justify-content-center">
            <img src="{{asset('images/logo.jpeg')}}" class="center" style=" border-radius: 50%;">
        </div>
        <h1><i class="fa fa-exclamation-circle"></i> Error 403: Page not found</h1>
        <p>The page you have requested is not found.</p>
        <p><a class="btn btn-primary" href="{{route('login')}}">Click to return</a></p>
    </div>
@endsection
