@extends('layouts.app')

@section('content')
    @can('isUser')
        <div class="row justify-content-center">
            @foreach($services as $service)
                <div class="col-sm-3 mb-1">
                    <div class="card small" >
                        @if(empty($service->picture->photo))
                            <img class="bd-placeholder-img card-img-top" width="100%" height="180" src="{{asset('landing/assets/img/book.jpg')}}">
                        @else
                            <a class="example-image-link" href="/storage/avatars/{{$service->picture->photo}}" data-lightbox="example-1">   <img class="bd-placeholder-img card-img-top" width="100%" height="180"  src="/storage/avatars/{{$service->picture->photo}}">
                        @endif

                        <div class="card-body">
                            <h5 class="card-title">{{$service->name}}</h5>
                            <p class="text-primary">{{$service->start_time}} --------- {{$service->end_time}}</p>
                            <div class="d-flex justify-content-between">
                                <p >Capacity: <strong>{{$service->capacity}} members</strong></p>
                                <p >Remaining: <strong>{{$service->capacity-$service->booking->where('status','Active')->count()}} spaces</strong></p>
                            </div>
                            @if((($service->capacity)-($service->booking->where('status','Active')->count()))>0)
                                @if(($service->booking->where('service_id',$service->id)->where('user_id',Auth::user()->id)->where('status','Active')->count())<=0)
                                    <a href="#" class="btn btn-primary btn-sm" id="submit{{$service->id}}" onclick="book('{{$service->id}}','submit{{$service->id}}')">Book Now!</a>
                                @else
                                    <a href="#" class="btn btn-secondary btn-sm"  disabled="disabled">Booked!</a>
                                @endif
                            @else
                                <a href="#" class="btn btn-warning btn-sm" disabled="disabled">Full!</a>
                            @endif

                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endcan

@endsection
