@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="tile">
                @include('modals.updateprofile')
{{--                @include('includes.message')--}}
                <div class="tile-title">
                    <p class="float-left">Profile Data</p>
                    <a class="btn btn-info fa fa-edit btn-sm float-right" data-toggle="modal" data-target="#update"></a>
                </div>
                <div class="tile-body">
                    <table class="table table-hover table-bordered" >
                        <thead>
                        <tr>

                            <th>Name</th>
                            <th>Data </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>First Name</td>
                            <td>{{$user->fname}}</td>
                        </tr>
                        <tr>
                            <td>Second Name</td>
                            <td>{{$user->sname}}</td>
                        </tr>
                        <tr>
                            <td>Surname</td>
                            <td>{{$user->surname}}</td>
                        </tr>
                        <tr>
                            <td>Phone Number</td>
                            <td>{{$user->phone}}</td>
                        </tr>
                        <tr>
                            <td>User Type</td>
                            <td>{{$user->role}}</td>
                        </tr>
                        <tr>
                            <td>Gender</td>
                            <td>{{$user->gender}}</td>
                        </tr>
                        <tr>
                            <td>Age</td>
                            <td>{{$user->age}}</td>
                        </tr>
                        <tr>
                            <td>Zone</td>
                            <td>{{$user->zone}}</td>
                        </tr>
                        <tr>
                            <td>Number of services booked</td>
                            <td>{{$user->booking->count()}}</td>
                        </tr>
                        <tr>
                            <td>Date registered</td>
                            <td>{{$user->created_at}}</td>
                        </tr>
                        <tr>
                            <td>Status</td>
                            <td>{{$user->status}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
