@extends('layouts.app')

@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
            <p>Welcome <strong class="text-info">{{Auth::user()->fname}}</strong></p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="#">{{Auth::user()->fname}}</a></li>
        </ul>
    </div>

    @can('isUser')
    <div class="row justify-content-center">
        @foreach($services as $service)
        <div class="col-sm-3 mb-1">
        <div class="card small" >
            @if(empty($service->picture->photo))
                 <img class="bd-placeholder-img card-img-top" width="100%" height="180" src="{{asset('landing/assets/img/book.jpg')}}">
            @else
                <a class="example-image-link" href="/storage/avatars/{{$service->picture->photo}}" data-lightbox="example-1">   <img class="bd-placeholder-img card-img-top" width="100%" height="180"  src="/storage/avatars/{{$service->picture->photo}}">
                @endif

            <div class="card-body">
                <h5 class="card-title">{{$service->name}}</h5>
                <p class="text-primary">{{$service->start_time}} --------- {{$service->end_time}}</p>
                <div class="d-flex justify-content-between">
                <p >Capacity: <strong>{{$service->capacity}} members</strong></p>
                <p >Remaining: <strong>{{$service->capacity-$service->booking->where('status','Active')->count()}} spaces</strong></p>
                </div>
                @if((($service->capacity)-($service->booking->where('status','Active')->count()))>0)
                    @if(($service->booking->where('service_id',$service->id)->where('user_id',Auth::user()->id)->where('status','Active')->count())<=0)
                <a href="#" class="btn btn-primary btn-sm" id="submit{{$service->id}}" onclick="book('{{$service->id}}','submit{{$service->id}}')">Book Now!</a>
                    @else
                <a href="#" class="btn btn-secondary btn-sm"  disabled="disabled">Booked!</a>
                    @endif
                @else
                    <a href="#" class="btn btn-warning btn-sm" disabled="disabled">Full!</a>
                @endif

            </div>
        </div>
    </div>
            @endforeach
    </div>
        @endcan



    @can('isAdmin')
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <div class="widget-small primary coloured-icon"><i class="icon fa fa-users fa-3x"></i>
                        <div class="info">
                            <h4>Active Users</h4>
                            <p><b>{{$active}}</b></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="widget-small danger coloured-icon"><i class="icon fa fa-users fa-3x"></i>
                        <div class="info">
                            <h4>Inactive Users</h4>
                            <p><b>{{$inactive}}</b></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="widget-small warning coloured-icon"><i class="icon fa fa-files-o fa-3x"></i>
                        <div class="info">
                            <h4>Services</h4>
                            <p><b>{{$servicescount}}</b></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="tile">
                        <h3 class="tile-title">Weekly Attendance</h3>
                        <div class="embed-responsive embed-responsive-16by9">
                            <canvas class="embed-responsive-item" id="lineChartDemo"></canvas>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="tile">
                        <h3 class="tile-title">Gender</h3>
                        <div class="embed-responsive embed-responsive-16by9">
                            <canvas class="embed-responsive-item" id="pieChartDemo"></canvas>
                        </div>
                    </div>
                </div>
            </div>
    @endcan
@endsection
