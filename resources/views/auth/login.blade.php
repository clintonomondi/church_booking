@extends('layouts.common')

@section('content')
    <section class="material-half-bg">
        <div class="cover"></div>
    </section>
    <section class="login-content">
        <div class="logo">
            <h1>LCC-Nakuru</h1>
        </div>
        <div class="login-box">
            <form class="login-form" action="{{route('login')}}" method="post" onsubmit="loadbutton()">
                @csrf
                <h3 class="login-head"><i class="fa fa-lg fa-fw fa-user"></i>SignIn to book</h3>
                <div class="form-group">
                    <label class="control-label">PHONE NO</label>
                    <input class="form-control" name="email" type="text" placeholder="Phone" autofocus>
                </div>
                    <input class="form-control" name="password"  type="password" value="admin" placeholder="Password" hidden>
                <div class="form-group">
                    <div class="utility">
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="materialContactFormCopy">
                            <label class="form-check-label" for="materialContactFormCopy">Stay signed in</label>
                        </div>
{{--                        <p class="semibold-text mb-2"><a href="#" data-toggle="flip">Forgot Password?</a></p>--}}
                        <p class="semibold-text mb-2 ml-1"><a href="#" data-toggle="modal" data-target="#modalRegisterForm">SignUp</a></p>
                    </div>
                </div>

                <div class="form-group btn-container">
                    <button class="btn btn-primary btn-block" id="btnloader" ><i class="fa fa-sign-in fa-lg fa-fw"></i>SIGN IN</button>
                </div>
            </form>
            <form class="forget-form" action="index.html">
                <h3 class="login-head"><i class="fa fa-lg fa-fw fa-lock"></i>Forgot Password ?</h3>
                <div class="form-group">
                    <label class="control-label">EMAIL</label>
                    <input class="form-control" type="text" placeholder="Email">
                </div>
                <div class="form-group btn-container">
                    <button class="btn btn-primary btn-block"><i class="fa fa-unlock fa-lg fa-fw"></i>RESET</button>
                </div>
                <div class="form-group mt-3">
                    <p class="semibold-text mb-0"><a href="#" data-toggle="flip"><i class="fa fa-angle-left fa-fw"></i> Back to Login</a></p>
                </div>
            </form>
        </div>
    </section>
    @include('modals.landingreg')
@endsection
