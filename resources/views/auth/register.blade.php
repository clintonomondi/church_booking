@extends('layouts.common')

@section('content')
    <section class="material-half-bg">
        <div class="cover"></div>
    </section>
    <section class="login-content">
        <div class="logo">
            <h1>LCC-Nakuru</h1>
        </div>
        <div class="login-box">
            <form class="login-form" action="{{route('login')}}" method="post">
                @csrf
                <h3 class="login-head"><i class="fa fa-lg fa-fw fa-user"></i>SIGN UP</h3>
                <div class="form-group">
                    <label class="control-label">NAME</label>
                    <input class="form-control" name="email" type="text" placeholder="Phone" autofocus>
                </div>
                <div class="form-group">
                    <label class="control-label">PHONE NO</label>
                    <input class="form-control" name="email" type="text" placeholder="Phone" autofocus>
                </div>
                <div class="form-group">
                    <label class="control-label">EMAIL</label>
                    <input class="form-control" name="email" type="text" placeholder="Phone" autofocus>
                </div>
                <div class="form-group">
                    <label class="control-label">ZONE</label>
                    <input class="form-control" name="email" type="text" placeholder="Phone" autofocus>
                </div>
                <div class="form-group">
                    <label class="control-label">AGE</label>
                    <input class="form-control" name="email" type="text" placeholder="Phone" autofocus>
                </div>
                <div class="form-group">
                    <label class="control-label">PASSWORD</label>
                    <input class="form-control" name="password" type="password" placeholder="Password">
                </div>
                @include('includes.message2')
                <div class="form-group">
                    <div class="utility">
                        <div class="animated-checkbox">
                            <label>
                                <input type="checkbox"><span class="label-text">Stay Signed in</span>
                            </label>
                        </div>
                        <p class="semibold-text mb-2"><a href="#" data-toggle="flip">Forgot Password ?</a></p>
                    </div>
                </div>
                <div class="form-group btn-container">
                    <button class="btn btn-primary btn-block"><i class="fa fa-sign-in fa-lg fa-fw"></i>SIGN IN</button>
                </div>
            </form>
            <form class="forget-form" action="index.html">
                <h3 class="login-head"><i class="fa fa-lg fa-fw fa-lock"></i>Forgot Password ?</h3>
                <div class="form-group">
                    <label class="control-label">EMAIL</label>
                    <input class="form-control" type="text" placeholder="Email">
                </div>
                <div class="form-group btn-container">
                    <button class="btn btn-primary btn-block"><i class="fa fa-unlock fa-lg fa-fw"></i>RESET</button>
                </div>
                <div class="form-group mt-3">
                    <p class="semibold-text mb-0"><a href="#" data-toggle="flip"><i class="fa fa-angle-left fa-fw"></i> Back to Login</a></p>
                </div>
            </form>
        </div>
    </section>
@endsection
