<!-- Modal -->
<div class="modal fade" id="book{{$service->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                {{--                <h5 class="modal-title" id="exampleModalLabel">Sign Up</h5>--}}
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form class="login-form" action="{{route('admin.book',$service->id)}}" method="post" enctype="multipart/form-data" onsubmit="loadbutton3('submit2{{$service->id}}')">
                    @csrf
                    <h3 class="login-head"><i class=""></i>Book {{$service->name}} {{$service->start_time}} --{{$service->end_time}}</h3>
                    <div class="form-group">
                        <h4 class="mb-4">You can select many users</h4>
                        <select class="form-control demoSelect mb-2"   name="user_id" style="width: 400px;">
                            <option disabled="disabled" id="sel1{{$service->id}}" selected="selected">Select User</option>
                                @foreach($users as $user)
                                <option value="{{$user->id}}">{{$user->fname}} {{$user->sname}} {{$user->surname}}</option>
                                    @endforeach
                        </select>
<input type="text" value="{{$service->id}}" name="service_id" hidden>
                    </div>

                    <div class="form-group">
                        <div class="utility">

                        </div>
                    </div>
                    <div class="form-group btn-container">
                        <button class="btn btn-primary btn-block" id="submit2{{$service->id}}"><i class="fa fa-sign-in fa-lg fa-fw"></i>Book</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
