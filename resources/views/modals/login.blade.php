<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Sign In</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="login-form" action="{{route('login')}}" method="post" onsubmit="loadbutton()">
                @csrf
                <div class="modal-body mx-3">


                    <div class="md-form mb-4">
                        <i class="fas fa-phone prefix grey-text"></i>
                        <input type="text" id="orangeForm-email"  name="phone" class="form-control validate">
                        <label data-error="wrong" data-success="right" for="orangeForm-email">Phone no.</label>
                    </div>
                    <div class="form-group">
                        <input class="form-control" name="password" type="password" value="admin" placeholder="Password" hidden>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="submit" id="btnloader" class="btn btn-deep-orange">Login</button>
                </div>
            </form>
        </div>
    </div>
</div>
