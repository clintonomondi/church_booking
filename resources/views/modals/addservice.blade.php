<!-- Modal -->
<div class="modal fade" id="service" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                {{--                <h5 class="modal-title" id="exampleModalLabel">Sign Up</h5>--}}
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form class="login-form" action="{{route('admin.addservice')}}" method="post" enctype="multipart/form-data" onsubmit="loadbutton()">
                    @csrf
                    <h3 class="login-head"><i class=""></i>Service</h3>
                    <div class="form-group">

                        <input class="form-control" name="name" type="text" placeholder="Name">
                    </div>
                    <div class="form-group">

                        <input class="form-control" name="start_time" type="text" placeholder="Start Time" >
                    </div>
                    <div class="form-group">

                        <input class="form-control" name="end_time" type="text" placeholder="End Time" >
                    </div>
                    <div class="form-group">

                        <input class="form-control" name="capacity" type="number" placeholder="Capacity" >
                    </div>
                    <div class="form-group">
<div class="row">
    <div class="col-sm-6">
        <img class="img-responsive img-thumbnail" id="blah" src="{{asset('images/user.png')}}"  alt="profile picture" style="width: 50%" />
    </div>
    <div class="col-sm-6">
        <input class="form-control" name="image" type="file" onchange="readURL(this);" placeholder="Capacity" >
    </div>
</div>
                    </div>

                    <div class="form-group">
                        <div class="utility">

                        </div>
                    </div>
                    <div class="form-group btn-container">
                        <button class="btn btn-primary btn-block" id="btnloader"><i class="fa fa-sign-in fa-lg fa-fw"></i>Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
