<div class="modal fade" id="modalRegisterForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Sign up</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="login-form" action="#" method="post" id="register" onsubmit="return false">
                @csrf
            <div class="modal-body mx-3">

                <div class="md-form mb-2">
                    <i class="fas fa-user prefix grey-text"></i>
                    <input type="text" id="orangeForm-name" name="fname" class="form-control validate">
                    <label data-error="wrong" data-success="right" for="orangeForm-name">First name</label>
                </div>
                <div class="md-form mb-2">
                    <i class="fas fa-user prefix grey-text"></i>
                    <input type="text" id="orangeForm-email"  name="sname" class="form-control validate">
                    <label data-error="wrong" data-success="right" for="orangeForm-email">Second name</label>
                </div>
                <div class="md-form mb-2">
                    <i class="fas fa-user prefix grey-text"></i>
                    <input type="text" id="orangeForm-email"  name="surname" class="form-control validate">
                    <label data-error="wrong" data-success="right" for="orangeForm-email">Surname</label>
                </div>
                <div class="md-form mb-2">
                    <i class="fas fa-phone prefix grey-text"></i>
                    <input type="text" id="orangeForm-email"  name="phone" class="form-control validate">
                    <label data-error="wrong" data-success="right" for="orangeForm-email">Phone no.</label>
                </div>

                <div class="md-form mb-2">
                    <i class="fas fa-mars prefix grey-text"></i>
                    <input type="text" id="orangeForm-email"  name="age" class="form-control validate">
                    <label data-error="wrong" data-success="right" for="orangeForm-email">Age</label>
                </div>
                <div class="md-form mb-2">
                    <i class="fas fa-home prefix grey-text"></i>
                    <input type="text" id="orangeForm-email"   name="zone"class="form-control validate">
                    <label data-error="wrong" data-success="right" for="orangeForm-email">Zone</label>
                </div>
                <div class="md-form mb-2">
                    <select class="form-control" id="exampleSelect1" name="gender">
                        <option disabled="disabled" selected="selected">Select gender</option>
                        <option>Male</option>
                        <option>Female</option>
                    </select>

                </div>
                <div class="form-group">
                    <input class="form-control" name="password" type="password" value="admin" placeholder="Password" hidden>
                </div>
                <div class="form-group">
                    <input class="form-control" name="repassword" type="password" value="admin" placeholder="Confirm password" hidden>
                </div>

            </div>
            <div class="modal-footer d-flex justify-content-center">
                <button type="submit" id="submit" class="btn btn-deep-orange">Sign up</button>
            </div>
            </form>
        </div>
    </div>
</div>
