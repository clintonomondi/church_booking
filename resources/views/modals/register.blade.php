<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
{{--                <h5 class="modal-title" id="exampleModalLabel">Sign Up</h5>--}}
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form class="login-form" action="#" method="post" id="register" onsubmit="return false">
                    @csrf
                    <h3 class="login-head"><i class="fa fa-sm fa-fw fa-user"></i>SIGN UP</h3>
                    <div class="form-group">
                        <input class="form-control" name="fname" type="text" placeholder="First Name" autofocus>
                    </div>
                    <div class="form-group">
                        <input class="form-control" name="sname" type="text" placeholder="Second Name" autofocus>
                    </div>
                    <div class="form-group">
                        <input class="form-control" name="surname" type="text" placeholder="Surname Name" autofocus>
                    </div>
                    <div class="form-group">

                        <input class="form-control" name="phone" type="text" placeholder="Phone" autofocus>
                    </div>
                    <div class="form-group">
                        <select class="form-control" id="exampleSelect1" name="gender">
                            <option disabled="disabled" selected="selected">Select gender</option>
                            <option>Male</option>
                            <option>Female</option>
                        </select>
                    </div>
                    <div class="form-group">

                        <input class="form-control" name="age" type="number" placeholder="Age" autofocus>
                    </div>
                    <div class="form-group">

                        <input class="form-control" name="zone" type="text" placeholder="Zone" autofocus>
                    </div>
                    <div class="form-group">
                        <input class="form-control" name="password" type="password" value="admin" placeholder="Password" hidden>
                    </div>
                    <div class="form-group">
                        <input class="form-control" name="repassword" type="password" value="admin" placeholder="Confirm password" hidden>
                    </div>
                    <div class="form-group">
                        <div class="utility">

                        </div>
                    </div>
                    <div class="form-group btn-container">
                        <button class="btn btn-primary btn-block" id="submit"><i class="fa fa-sign-in fa-lg fa-fw"></i>Submit</button>
                    </div>
                </form>
            </div>
{{--            <div class="modal-footer">--}}
{{--                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>--}}
{{--                <button type="button" class="btn btn-primary">Submit</button>--}}
{{--            </div>--}}
        </div>
    </div>
</div>
