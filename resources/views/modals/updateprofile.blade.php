<!-- Modal -->
<div class="modal fade" id="update" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                {{--                <h5 class="modal-title" id="exampleModalLabel">Sign Up</h5>--}}
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form class="login-form" action="{{route('updateprofile')}}" method="post" onsubmit="loadbutton()">
                    @csrf
                    <div class="form-group">
                        <input class="form-control" name="fname" type="text" placeholder="First Name" value="{{$user->fname}}" autofocus>
                    </div>
                    <div class="form-group">
                        <input class="form-control" name="sname" type="text" placeholder="Second Name" value="{{$user->sname}}" autofocus>
                    </div>
                    <div class="form-group">
                        <input class="form-control" name="surname" type="text" placeholder="Surname Name" value="{{$user->surname}}" autofocus>
                    </div>
                    <div class="form-group">

                        <input class="form-control" name="phone" type="text" placeholder="Phone" value="{{$user->phone}}" autofocus disabled>
                    </div>
                    <div class="form-group">
                        <select class="form-control" id="exampleSelect1" name="gender">
                            <option>{{$user->gender}}</option>
                            <option>Male</option>
                            <option>Female</option>
                        </select>
                    </div>
                    <div class="form-group">

                        <input class="form-control" name="age" type="number" placeholder="Age" value="{{$user->age}}" autofocus>
                    </div>
                    <div class="form-group">

                        <input class="form-control" name="zone" type="text" placeholder="Zone" value="{{$user->zone}}" autofocus>
                    </div>
                    <div class="form-group">
                        <div class="utility">

                        </div>
                    </div>
                    <div class="form-group btn-container">
                        <button class="btn btn-primary btn-block" id="btnloader"><i class="fa fa-sign-in fa-lg fa-fw"></i>Update</button>
                    </div>
                </form>
            </div>
            {{--            <div class="modal-footer">--}}
            {{--                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>--}}
            {{--                <button type="button" class="btn btn-primary">Submit</button>--}}
            {{--            </div>--}}
        </div>
    </div>
</div>
