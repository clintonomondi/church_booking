<!-- Modal -->
<div class="modal fade" id="reset" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                {{--                <h5 class="modal-title" id="exampleModalLabel">Sign Up</h5>--}}
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to reset this bookings?</p>
                    <div class="form-group btn-container">
                        <a class="btn btn-primary btn-sm" onclick="loadbutton3('btnloader2')" id="btnloader2" href="{{route('booking.reset')}}"><i class="fa fa-sign-in fa-lg fa-fw"></i>RESET</a>
                    </div>

            </div>
        </div>
    </div>
</div>
