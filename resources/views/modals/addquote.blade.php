<!-- Modal -->
<div class="modal fade" id="quote" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                {{--                <h5 class="modal-title" id="exampleModalLabel">Sign Up</h5>--}}
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form class="login-form" action="{{route('admin.addquote')}}" method="post" enctype="multipart/form-data" onsubmit="loadbutton()">
                    @csrf
                    <h3 class="login-head"><i class=""></i>Add Quote</h3>
                    <p>Note-This will be the active quote viewed by users</p>
                    <div class="form-group">
                        <input class="form-control" name="verse" type="text" placeholder="Verse">
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <textarea class="form-control" rows="4" name="quote" placeholder="Type your quote"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="utility">

                        </div>
                    </div>
                    <div class="form-group btn-container">
                        <button class="btn btn-primary btn-block" id="btnloader"><i class="fa fa-sign-in fa-lg fa-fw"></i>Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
