@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="tile">
                <div class="tile-title">
                    @if(empty($to))
                    <p id="dataname">Bookings Reports</p>
                        @else
                        <p id="dataname">Bookings Reports  {{$from}}-----{{$to}}</p>
                    @endif
                </div>
                <div class="tile-body">

                        <form method="post" id="reportform" name="reportform" action="{{route('report.getservicedata')}}">
                            @csrf
                            <div class="row justify-content-center">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input class="form-control" name="from" type="text" placeholder="Date from" onfocus="(this.type='date')" required>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">

                                    <input class="form-control" name="to" type="text" placeholder="Date to" onfocus="(this.type='date')" required>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                        <select class="form-control" id="sel1" name="service_id"  onchange="submitform2()">
                                             <option disabled="disabled" selected="selected">Select Service</option>
                                            @foreach($services as $service)
                                                <option value="{{$service->id}}">{{$service->name}}</option>
                                            @endforeach
                                          </select>
                                </div>
                            </div>
                    </div>
                        </form>

                    <table class="table table-hover table-bordered" id="table">
                        <thead>
                        <tr>

                            <th>#</th>
                            <th>Service Name</th>
                            <th>User Name</th>
                            <th>User Phone</th>
                            <th>Start</th>
                            <th>End </th>
                            <th>Seat</th>
                            <th>Status</th>
                            <th>Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($bookings as $key=>$booking)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$booking->service->name}}</td>
                                <td>{{$booking->user->fname}} {{$booking->user->sname}}</td>
                                <td>{{$booking->user->phone}}</td>
                                <td>{{$booking->service->start_time}}</td>
                                <td>{{$booking->service->end_time}}</td>
                                <td>{{$booking->chair}}</td>
                                @if($booking->status=='Active')
                                    <td class="text-success">{{$booking->status}}</td>
                                @else
                                    <td class="text-info">{{$booking->status}}</td>
                                @endif
                                <td>{{$booking->created_at}}</td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
