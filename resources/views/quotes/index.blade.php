@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-title">
                    <p>Weekly Quotes</p>
                    <a class="btn btn-primary float-right  btn-sm" data-toggle="modal" data-target="#quote">Add Quote</a>
                </div>
                @include('modals.addquote')
                <div class="tile-body">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Quote</th>
                            <th>Verse</th>
                            <th>Created date</th>
                            <th>Status</th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($datas as $key=>$data)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$data->quote}}</td>
                                <td>{{$data->verse}}</td>
                                <td>{{$data->created_at}}</td>
                                @if($data->status=='Active')
                                <td class="text-success">{{$data->status}} </td>
                                    <td></td>
                                @else
                                    <td class="text-danger">{{$data->status}} </td>
                                    <td><a class="btn btn-success fa fa-home btn-sm" href="{{route('admin.quote.activate',$data->id)}}">Activate</a></td>
                                @endif

                                <td><a class="btn btn-info fa fa-edit btn-sm" data-toggle="modal" data-target="#quote{{$data->id}}">Edit</a></td>
                            </tr>
@include('modals.editquote')
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
