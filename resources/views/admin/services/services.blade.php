@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-title">
                    <p>Services</p>

                        <a class="btn btn-primary float-right  btn-sm" data-toggle="modal" data-target="#service">Add service</a>
                        <a class="btn btn-secondary float-left btn-sm" data-toggle="modal" data-target="#reset">Reset</a>

                </div>
@include('modals.addservice')
@include('modals.reset')
                <div class="tile-body">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Start Time</th>
                            <th>End Time</th>
                            <th>Capacity</th>
                            <th>Booked</th>
                            <th>Empty</th>
                            <th>Book</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($services as $key=>$service)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$service->name}}</td>
                                <td>{{$service->start_time}}</td>
                                <td>{{$service->end_time}}</td>
                                <td>{{$service->capacity}} Chairs</td>
                                <td><a href="{{route('booking.view',$service->id)}}">{{$service->booking->where('status','Active')->count()}} spaces</a></td>
                                <td>{{($service->capacity)-($service->booking->where('status','Active')->count())}} left</td>
                                <td><a class="btn btn-info fa fa-edit btn-sm" data-toggle="modal" data-target="#service{{$service->id}}">Edit</a></td>
                                <td><a class="btn btn-primary fa fa-home btn-sm" href="{{route('admin.bookings',$service->id)}}">Book</a></td>
                            </tr>
                            @include('modals.editservice')
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
