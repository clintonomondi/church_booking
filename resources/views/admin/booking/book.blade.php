@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-title">
                    <p>Book {{$service->name}}     {{$service->start_time}}  --  {{$service->end_time}}</p>
                </div>
                <div class="tile-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <a class="example-image-link" href="/storage/avatars/{{$service->picture->photo}}" data-lightbox="example-1">   <img class="" width="100%" height="100%"  src="/storage/avatars/{{$service->picture->photo}}"></a>
                        </div>
                        <div class="col-sm-6">
                            <form class="login-form" action="{{route('admin.book',$service->id)}}" method="post" enctype="multipart/form-data" onsubmit="loadbutton3('submit2{{$service->id}}')">
                                @csrf
                                <div class="form-group">
                                    <p>You can select many users</p>
                                    <select class="form-control demoSelect mb-2"  multiple="multiple"  name="user_id[]">
{{--                                        <option disabled="disabled" id="sel1" selected="selected">Select User</option>--}}
                                        @foreach($users as $user)
                                            <option value="{{$user->id}}">{{$user->fname}} {{$user->sname}} {{$user->surname}}--- {{$user->phone}}</option>
                                        @endforeach
                                    </select>
                                    <input type="text" value="{{$service->id}}" name="service_id" hidden>
                                </div>

                                <div class="form-group">
                                    <div class="utility">

                                    </div>
                                </div>
                                <div class="form-group btn-container">
                                    <button class="btn btn-primary btn-sm" id="submit2{{$service->id}}"><i class="fa fa-sign-in fa-lg fa-fw"></i>Book</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>



    <div class="row">
        <div class="col-sm-12">
            <div class="tile">
                <div class="tile-title">
                    <p id="dataname">Current Active booking</p>
                </div>
                <div class="tile-body">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                        <tr>

                            <th>Service Name</th>
                            <th>User Name</th>
                            <th>User Phone</th>
                            <th>Start</th>
                            <th>End </th>
                            <th>Seat</th>
                            <th>Status</th>
                            <th>Date</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($service->booking->where('status','Active') as $key=>$booking)
                            <tr>

                                <td>{{$booking->service->name}}</td>
                                <td>{{$booking->user->fname}} {{$booking->user->sname}}</td>
                                <td>{{$booking->user->phone}}</td>
                                <td>{{$booking->service->start_time}}</td>
                                <td>{{$booking->service->end_time}}</td>
                                <td>{{$booking->chair}}</td>
                                @if($booking->status=='Active')
                                    <td class="text-success">{{$booking->status}}</td>
                                    <td><a href="#" class="btn btn-primary btn-sm" id="submit{{$booking->id}}" onclick="cancel('{{$booking->id}}','submit{{$booking->id}}')">Cancel</a></td>
                                @else
                                    <td class="text-info">{{$booking->status}}</td>
                                    <td></td>
                                @endif
                                <td>{{$booking->created_at}}</td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
