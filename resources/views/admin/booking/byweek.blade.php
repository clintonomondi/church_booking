@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="tile">
                <div class="tile-title">
                    <p id="dataname">Weekly Summery</p>
                </div>
                <div class="tile-body">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                        <tr>

                            <th>#</th>
                            <th>Week</th>
                            <th>Year</th>
                            <th>Members</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($weeks as $key=>$week)
                            <tr>
                                <th>{{$key+1}}</th>
                                <td>{{$week->weekn}}</td>
                                <td>{{$week->mwaka}}</td>
                                <td>{{$week->members}}</td>
                                <td> <a href="{{route('report.byweekdata',$week->weekn)}}" class="btn btn-primary btn-sm fa fa-eye" >View</a></td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
