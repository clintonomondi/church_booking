@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="tile">
                <div class="tile-title">
                    <p id="dataname">{{$service->name}} {{$service->start_time}} --- {{$service->end_time}}</p>
                </div>
                <div class="tile-body">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                        <tr>

                            <th>Service Name</th>
                            <th>User Name</th>
                            <th>User Phone</th>
                            <th>Start</th>
                            <th>End </th>
                            <th>Seat</th>
                            <th>Status</th>
                            <th>Date</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($bookings as $key=>$booking)
                            <tr>

                                <td>{{$booking->service->name}}</td>
                                <td>{{$booking->user->fname}} {{$booking->user->sname}}</td>
                                <td>{{$booking->user->phone}}</td>
                                <td>{{$booking->service->start_time}}</td>
                                <td>{{$booking->service->end_time}}</td>
                                <td>{{$booking->chair}}</td>
                                @if($booking->status=='Active')
                                    <td class="text-success">{{$booking->status}}</td>
                                    <td><a href="#" class="btn btn-primary btn-sm" id="submit{{$booking->id}}" onclick="cancel('{{$booking->id}}','submit{{$booking->id}}')">Cancel</a></td>
                                @else
                                    <td class="text-info">{{$booking->status}}</td>
                                    <td></td>
                                @endif
                                <td>{{$booking->created_at}}</td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
