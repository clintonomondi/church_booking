@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="tile">
                <div class="tile-title">
                    <p id="dataname">Users</p>
                    <a class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#exampleModal">Add Users</a>
                </div>
                @include('modals.register')
                <div class="tile-body">
                    <form method="post" id="reportform" name="reportform" action="{{route('admin.searchuser')}}">
                        @csrf
                    <div class="row justify-content-center">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <select class="form-control" id="sel1r" name="user_id" onchange="submitform2()">
                                    <option disabled="disabled" selected="selected">Select User</option>
                                    <option value="0">All</option>
                                    @foreach($users_to_loop as $user)
                                        <option value="{{$user->id}}">{{$user->fname}} {{$user->sname}} {{$user->surname}} ---{{$user->phone}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    </form>
                    <table class="table table-hover table-bordered" id="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>First name</th>
                            <th>Second  name</th>
                            <th>Surname</th>
                            <th>Phone</th>
                            <th>Gender</th>
                            <th>Age</th>
                            <th>Zone</th>
                            <th>Status</th>
                            <th>Date joined</th>
                            <th>Role</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $key=>$user)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$user->fname}}</td>
                            <td>{{$user->sname}}</td>
                            <td>{{$user->surname}}</td>
                            <td>{{$user->phone}}</td>
                            <td>{{$user->gender}}</td>
                            <td>{{$user->age}}</td>
                            <td>{{$user->zone}}</td>
                            @if($user->status=='Active')
                            <td class="text-success">{{$user->status}}</td>
                            @else
                                <td class="text-warning">{{$user->status}}</td>
                                @endif
                            <td>{{$user->created_at}}</td>
                            <td>
                                <select class="form-control" id="sel1" name="role" onchange="updateRole(this.value,'{{$user->id}}')">
                                    <option>{{$user->role}}</option>
                                    <option class="text-success">admin</option>
                                    <option class="text-danger">user</option>
                                </select>
                            </td>
                            <td><a class="btn btn-info fa fa-edit btn-sm" data-toggle="modal" data-target="#service{{$user->id}}">Edit</a></td>
                        </tr>
                        @include('modals.edituser')
                        @endforeach

                        </tbody>
                    </table>
                    {{ $users->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
