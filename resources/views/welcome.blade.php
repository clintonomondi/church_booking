<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="ICON" href="{{asset('images/logo.jpeg')}}" type="image/ico" />
    <title>LCC-Booking</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="{{asset('landing/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="{{asset('landing/css/mdb.min.css')}}" rel="stylesheet">
    <style>
        html,
        body,
        header,
        .jarallax {
            height: 100%;
        }

        @media (min-width: 560px) and (max-width: 740px) {

            html,
            body,
            header,
            .jarallax {
                height: 500px;
            }
        }

        @media (min-width: 800px) and (max-width: 850px) {

            html,
            body,
            header,
            .jarallax {
                height: 500px;
            }
        }

        @media (min-width: 800px) and (max-width: 850px) {
            .navbar:not(.top-nav-collapse) {
                background: #1C2A48 !important;
            }

            .navbar {
                box-shadow: 0 2px 5px 0 rgba(0, 0, 0, .16), 0 2px 10px 0 rgba(0, 0, 0, .12) !important;
            }
        }

    </style>
</head>

<body class="university-lp">

<!--Navigation & Intro-->
<header>

    <!--Navbar-->
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar">
        <div class="container">
            <a class="navbar-brand" href="#">
                <strong></strong>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!--Links-->
                <ul class="navbar-nav mr-auto smooth-scroll">
                    <li class="nav-item">
                        <a class="nav-link" href="/">Home
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-offset="100" data-toggle="modal" data-target="#modalRegisterForm">Sign up</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('login')}}" data-offset="100">Sign In</a>
                    </li>
                </ul>

                <!--Social Icons-->
                <ul class="navbar-nav nav-flex-icons">
                    <li class="nav-item">
                        <a class="nav-link">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!--Navbar-->
    @include('modals.landingreg')
    <!-- Intro Section -->
    <div id="home" class="view jarallax" data-jarallax='{"speed": 0.2}'
         style="background-image: url('images/landing.jpeg'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
        <div class="mask rgba-black-strong">
            <div class="container h-100 d-flex justify-content-center align-items-center">
                <div class="row smooth-scroll">
                    <div class="col-md-12 white-text text-center">
                        <div class="wow fadeInDown" data-wow-delay="0.2s">
                            <h2 class="display-4 mb-2">LCC-Nakuru</h2>
                            <hr class="hr-light">
                            <h4 class="subtext-header mt-4 mb-5">Church of choice transforming Nations for Christ!.</h4>
                            @if(!empty($quote))
                            <h2 class="h2-responsive mb-5">
                                <i class="fas fa-quote-left" aria-hidden="true"></i> {{$quote->quote}}
                                <i class="fas fa-quote-right" aria-hidden="true"></i>
                            </h2>
                            <h5 class="text-center font-italic " data-wow-delay="0.2s">~ {{$quote->verse}}</h5>
                                @endif
                        </div>
                        <a href="#" data-offset="100" class="btn btn-info wow fadeInLeft" data-wow-delay="0.2s" data-toggle="modal" data-target="#modalRegisterForm">Sign up</a>
                        <a href="{{route('login')}}" data-offset="100" class="btn btn-warning wow fadeInRight"
                           data-wow-delay="0.2s">Sign in</a>
                    </div>
                </div>
            </div>
        </div>
    </div>


</header>
<!--Navigation & Intro-->

<!--Main content-->
<main>

    <div class="container">

        <!--Section: About-->


    </div>

    <!--Streak-->
    <div class="streak streak-photo streak-md"
         style="background-image: url('https://mdbootstrap.com/img/Photos/Horizontal/Things/full%20page/img%20%287%29.jpg');">
        <div class="flex-center mask rgba-indigo-strong">
            <div class="text-center white-text">
                @if(!empty($quote))
                <h2 class="h2-responsive mb-5">
                    <i class="fas fa-quote-left" aria-hidden="true"></i> {{$quote->quote}}
                    <i class="fas fa-quote-right" aria-hidden="true"></i>
                </h2>
                <h5 class="text-center font-italic " data-wow-delay="0.2s">~ {{$quote->verse}}</h5>
                    @endif
            </div>
        </div>
    </div>
    <!--Streak-->


    <div class="container-fluid background-r">
        <div class="container py-3">

            <!--Section: Blog v.2-->
            <section class="extra-margins text-center">

                <h2 class="text-center mb-5 my-5 pt-4 pb-4 font-weight-bold"> Top Services</h2>

                <!--Grid row-->
                <div class="row mb-5 pb-3">

                @foreach($services as $service)
                    <!--Grid column-->
                    <div class="col-lg-3 col-md-6 mb-4 wow fadeIn" data-wow-delay="0.4s">

                        <!--Card Light-->
                        <div class="card">
                            <!--Card image-->
                            <div class="view overlay">
                                <img src="/storage/avatars/{{$service->picture->photo}}" class="card-img-top" alt="">
                                <a>
                                    <div class="mask rgba-white-slight"></div>
                                </a>
                            </div>
                            <!--/.Card image-->
                            <!--Card content-->
                            <div class="card-body">

                                <!--Title-->
                                <h4 class="card-title darkgrey-text">
                                    <strong>{{$service->name}}</strong>
                                </h4>
                                <hr>
                                <!--Text-->
                                <p class="font-small">{{$service->start_time}} TO {{$service->end_time}}
                                </p>
                                <a href="{{route('user.services')}}" class="black-text d-flex flex-row-reverse">
                                    <p class="waves-effect p-2 font-small blue-text mb-0">Book now
                                        <i class="fas fa-long-arrow-alt-right ml-2" aria-hidden="true"></i>
                                    </p>
                                </a>
                            </div>
                            <!--/.Card content-->
                        </div>
                        <!--/.Card Light-->

                    </div>
                    <!--Grid column-->

@endforeach

                </div>
                <!--First row-->

            </section>
            <!--Section: Blog v.2-->

        </div>
    </div>



</main>
<!--Main content-->

<!--Footer-->
<footer class="page-footer text-center text-md-left mdb-color darken-3">

    <!--Copyright-->
    <div class="footer-copyright py-3 text-center">
        <div class="container-fluid">
            © 2020 Copyright: <a href="/" target="_blank">LCC-Nakuru  </a>
        </div>
    </div>
    <!--Copyright-->

</footer>
<!--Footer-->

<!--SCRIPTS-->

<!--JQuery-->
<script type="text/javascript" src="{{asset('landing/js/jquery.min.js')}}"></script>

<!--Bootstrap tooltips-->
<script type="text/javascript" src="{{asset('landing/js/popper.min.js')}}"></script>

<!--Bootstrap core JavaScript-->
<script type="text/javascript" src="{{asset('landing/js/bootstrap.min.js')}}"></script>

<!--MDB core JavaScript-->
<script type="text/javascript" src="{{asset('landing/js/mdb.min.js')}}"></script>

<script>
    //Animation init
    new WOW().init();

    //Modal
    $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').focus()
    })

    // Material Select Initialization
    $(document).ready(function () {
        $('.mdb-select').material_select();
    });

</script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{asset('js/auth.js')}}"></script>
</body>

</html>
