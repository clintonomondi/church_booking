<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="ICON" href="{{asset('images/logo.jpeg')}}" type="image/ico" />
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('docs/css/main.css')}}">
    <!-- Font-icon css-->
{{--    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">--}}
    <title>Booking-Login</title>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="{{asset('landing/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="{{asset('landing/css/mdb.min.css')}}" rel="stylesheet">
</head>
<body>
@include('sweet::alert')
<main class="py-4">
    @yield('content')
</main>
<!-- Essential javascripts for application to work-->
<script src="{{asset('docs/js/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('docs/js/popper.min.js')}}"></script>
<script src="{{asset('docs/js/bootstrap.min.js')}}"></script>
<script src="{{asset('docs/js/main.js')}}"></script>
<!-- The javascript plugin to display page loading on top-->
<script src="{{asset('docs/js/plugins/pace.min.js')}}"></script>
<script type="text/javascript">
    // Login Page Flipbox control
    $('.login-content [data-toggle="flip"]').click(function() {
        $('.login-box').toggleClass('flipped');
        return false;
    });
</script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{asset('js/auth.js')}}"></script>
<!--JQuery-->
<script type="text/javascript" src="{{asset('landing/js/jquery.min.js')}}"></script>

<!--Bootstrap tooltips-->
<script type="text/javascript" src="{{asset('landing/js/popper.min.js')}}"></script>

<!--Bootstrap core JavaScript-->
<script type="text/javascript" src="{{asset('landing/js/bootstrap.min.js')}}"></script>

<!--MDB core JavaScript-->
<script type="text/javascript" src="{{asset('landing/js/mdb.min.js')}}"></script>

</body>
</html>
