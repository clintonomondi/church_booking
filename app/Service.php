<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected  $fillable=['name','start_time','end_time','capacity','created_by','updated_by'];

    public  function booking(){
        return $this->hasMany(Booking::class);
    }
    public  function picture(){
        return $this->hasOne(Picture::class);
    }
}
