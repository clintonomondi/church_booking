<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Service;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $status=User::find(Auth::user()->id);
        if($status->status!='Active'){
            return redirect('login')->with(Auth::logout())->with('error','You are not authorised to login!');
        }
        $active=User::where('status','Active')->count();
        $inactive=User::where('status','Inactive')->count();
        $servicescount=Service::count();
        $services=Service::all();
        $books=Booking::where('user_id',Auth::user()->id)->where('status','Active')->get();
        return view('home',compact('services','books','active','inactive','servicescount'));
    }

    public  function profile(){
        $user=User::where('phone',Auth::user()->phone)->first();
        return view('user.profile',compact('user'));
    }

    public  function updateprofile(Request $request){
        $validatedData = $request->validate([
            'fname' => 'required',
            'zone' => 'required',
            'age' => 'required',
        ]);
        if($request->age>=58){
            return redirect()->back()->with('errors','Age cannot be more than 58');
        }
        $user=User::find(Auth::user()->id);
        $user->update($request->all());
        return redirect()->back()->with('success','Profile data updated successfully');
    }

    public  function updateuser(Request $request,$id){
        if(empty($request->fane) || empty($request->phone)){
            return redirect()->route('admin.users')->with('errors','Please provide first name and phone number');
        }
        if($request->id==Auth::user()->id){
            return redirect()->route('admin.users')->with('errors','You cannot update this user, please go to profile to update');
        }
        if($request->age>=58){
            return redirect()->route('admin.users')->with('errors','Age cannot be more than 58');
        }
        $user=User::find($id);
        $user->update($request->all());
        return redirect()->route('admin.users')->with('success','Profile user data updated successfully');
    }

    public  function updateRole(Request $request){
        if(Auth::user()->id==$request->id){
            return ['status'=>false,'message'=>'You are not authorised to update this role'];
        }
        $user=User::find($request->id);
        $user->update($request->all());
        return ['status'=>true,'message'=>'Role updated successfully'];
    }

    public  function getData(){
        $male=User::where('gender','Male')->count();
        $female=User::where('gender','Female')->count();
        $other=User::where('gender','!=','Male')->where('gender','!=','Female')->count();

        $info = DB::select( DB::raw("SELECT  WEEK(created_at)AS created,
(SELECT COUNT(*) FROM bookings B WHERE WEEK(created_at)=created)number
  FROM `bookings` A GROUP BY created  ORDER BY created DESC  LIMIT 8") );
        $gender=array(
            'female'=>$female,
            'male'=>$male,
            'other'=>$other,
        );
        return['status'=>true,'gender'=>$gender,'info'=>$info];
    }
}
