<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public  function all(){
        $users=User::orderBy('status','asc')->paginate(100);
        $users_to_loop=User::all();
        return view('admin.users',compact('users','users_to_loop'));
    }

    public  function searchuser(Request $request){
        if($request->user_id=='0'){
            $users=User::orderBy('status','asc')->paginate(100);
        }else{
            $users=User::where('id',$request->user_id)->paginate(100);
        }

        $users_to_loop=User::all();
        return view('admin.users',compact('users','users_to_loop'));
    }

}
