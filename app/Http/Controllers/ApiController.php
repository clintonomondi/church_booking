<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ApiController extends Controller
{
    public  function register(Request $request){

        $validatedData = $request->validate([
            'fname' => 'required',
            'zone' => 'required',
            'sname' => 'required',
            'phone' => 'required',
            'age' => 'required',
        ]);
        if(($request->age)>=58){
            return ['status'=>false,'message'=>'Age cannot be 58 and above years'];
        }
        $isPhone=User::where('phone',$request->phone)->first();
        if(!empty($isPhone)){
            return ['status'=>false,'message'=>'The phone number is already in use'];
        }
        if ($request->password!== $request->repassword) {
            return ['status'=>false,'message'=>'Password are not matching'];
        }

        $request['password']=Hash::make($request->password);

        $data=User::create($request->all());
        return ['status'=>true,'message'=>'Registration successful, click login.....'];
    }
}
