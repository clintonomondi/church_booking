<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Picture;
use App\Service;
use App\ServiceImage;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ServiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public  function all(){
        $services=Service::all();
        return view('admin.services.services',compact('services'));
    }

    public  function user_all(){
        $services=Service::all();
        $books=Booking::where('user_id',Auth::user()->id)->where('status','Active')->get();
        return view('user.services',compact('services','books'));
    }

    public  function addservice(Request $request){
        $validatedData = $request->validate([
            'name' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'capacity' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1999',
        ]);
        $request['created_by']=Auth::user()->id;
        $data=Service::create($request->all());

        //get file name with extension
        $fileNameWithExt=$request->file('image')->getClientOriginalName();
        $filename=pathinfo($fileNameWithExt,PATHINFO_FILENAME);
        $extesion=$request->file('image')->getClientOriginalExtension();
        //filename to store
        $fileNameToStore=$filename.'_'.time().'.'.$extesion;
        //uploadimage
        $path=$request->file('image')->storeAs('/public/avatars',$fileNameToStore);

        $request['service_id']=$data->id;
        $request['photo']=$fileNameToStore;
        $date=Picture::create($request->all());
        return redirect()->back()->with('success', 'Service added successfully');
    }

    public  function updateservice(Request $request,$id){
        $service=Service::find($id);

        $validatedData = $request->validate([
            'name' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'capacity' => 'required',
        ]);
        if(!empty($request->image)){
            $validatedData = $request->validate([
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1999',
            ]);

            //get file name with extension
            $fileNameWithExt=$request->file('image')->getClientOriginalName();
            $filename=pathinfo($fileNameWithExt,PATHINFO_FILENAME);
            $extesion=$request->file('image')->getClientOriginalExtension();
            //filename to store
            $fileNameToStore=$filename.'_'.time().'.'.$extesion;
            //uploadimage
            $path=$request->file('image')->storeAs('/public/avatars',$fileNameToStore);
            $request['photo']=$fileNameToStore;
            $photo=Picture::where('service_id',$id)->first();
            $pic=Picture::find($photo->id);
            $pic->update($request->all());

        }
        $service->update($request->all());
        return redirect()->back()->with('success', 'Service updated successfully');
    }


    public  function book(Request $request){

        $check=Booking::where('user_id',Auth::user()->id)->where('status','Active')->first();
        if(!empty($check)){
            return ['status'=>false,'message'=>'The booking is already done!'];
        }
        $count=Booking::where('service_id',$request->service_id)->where('status','Active')->count();
        if($count<=0){
            $request['chair']='1';
        }else{
            $request['chair']=$count+1;
        }
        $request['user_id']=Auth::user()->id;
        $data=Booking::create($request->all());
        return ['status'=>true,'message'=>'You have successfully booked seat Number'.$request['chair'].' , Please wait....'];
    }

    public  function adminbooking($id){
        $users=User::all();
        $service=Service::find($id);
        return view('admin.booking.book',compact('users','service'));
    }

    public  function adminbook(Request $request){
        if(empty($request->user_id) || $request->user_id=='0'){
            return redirect()->back()->with('errors','Please select at least one user');
        }
          foreach ($request->user_id as $key=>$user_id) {
              $request['user_id'] = $user_id;
              $check = Booking::where('user_id', $user_id)->where('status', 'Active')->first();
              if (empty($check)) {
                  $count = Booking::where('service_id', $request->service_id)->where('status', 'Active')->count();
                  if ($count <= 0) {
                      $request['chair'] = '1';
                  } else {
                      $request['chair'] = $count + 1;
                  }
                  $data = Booking::create($request->all());
              } else {

              }
          }
        return redirect()->back()->with('success','Bookings done successfully');

    }

    public  function getUserBooking(){
        $bookings=Booking::orderby('id','desc')->where('user_id',Auth::user()->id)->get();
        return view('user.booking.booking',compact('bookings'));
    }

    public  function cancelbook(Request $request){
        $data=Booking::find($request->id);
        $request['status']='Cancelled';
        $data->update($request->all());
        return ['status'=>true,'message'=>'Service cancelled successfully, redirecting....'];
    }

    public  function reset(){
        $users = DB::select("UPDATE bookings set status='Reset' WHERE status='Active'");
        return redirect()->back()->with('success','Bookings reset successfully');
    }

    public  function bookings(){
        $bookings=Booking::orderBy('id','desc')->paginate(100);
        $users=User::all();
        return view('admin.booking.booking',compact('bookings','users'));
    }
    public  function searchbookings(Request $request){
        if($request->user_id=='0'){
            $bookings=Booking::orderBy('id','desc')->paginate(100);
        }else{
            $bookings=Booking::orderBy('id','desc')->where('user_id',$request->user_id)->paginate(100);
        }
        $users=User::all();
        return view('admin.booking.booking',compact('bookings','users'));
    }

public  function view($id){
        $bookings=Booking::where('service_id',$id)->where('status','Active')->get();
        $service=Service::find($id);
        return view('admin.booking.spaces',compact('bookings','service'));
}

}
