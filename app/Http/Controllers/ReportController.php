<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Service;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public  function services(){
        $bookings=Booking::orderBy('id','desc')->limit(100)->get();
        $services=Service::all();

        return view('report.service',compact('bookings','services'));
    }

    public  function users(){
        $bookings=Booking::orderBy('id','desc')->limit(100)->get();
        $users=User::all();

        return view('report.users',compact('bookings','users'));
    }

    public  function getservicedata(Request $request){
        $validatedData = $request->validate([
            'to' => 'required',
            'from' => 'required',
        ]);
        $bookings=Booking::orderBy('id','desc')->where('service_id',$request->service_id)->whereBetween('created_at', [$request->from, $request->to])->get();
        $services=Service::all();
        $to=$request->to;
        $from=$request->from;
        return view('report.service',compact('bookings','services','to','from'));
    }

    public  function getusersdata(Request $request){
        $validatedData = $request->validate([
            'to' => 'required',
            'from' => 'required',
        ]);
        $bookings=Booking::orderBy('id','desc')->where('user_id',$request->user_id)->whereBetween('created_at', [$request->from, $request->to])->get();
        $users=User::all();
        $to=$request->to;
        $from=$request->from;
        return view('report.users',compact('bookings','users','to','from'));
    }

    public  function byweek(){
        $weeks = DB::select( DB::raw("SELECT  WEEK(created_at)AS weekn,
(SELECT COUNT(*) FROM bookings B WHERE WEEK(created_at)=weekn)members,
(SELECT YEAR(created_at) FROM bookings B WHERE WEEK(created_at)=weekn LIMIT 1)mwaka
  FROM `bookings` A GROUP BY weekn  ORDER BY weekn DESC"));

        return view('admin.booking.byweek',compact('weeks'));
    }

    public  function byweekdata($week){
        $bookings =Booking::where(\DB::raw("WEEK(created_at)"), $week)->get();

        return view('admin.booking.byweekdata',compact('bookings','week'));
    }



}
