<?php

namespace App\Http\Controllers;

use App\Quote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class QuotesController extends Controller
{
    public  function index(){
        $datas=Quote::orderBy('status','asc')->get();
        return view('quotes.index',compact('datas'));
    }

    public  function addquote(Request $request){
        $validatedData = $request->validate([
            'verse' => 'required',
            'quote' => 'required',
        ]);

        $request['created_by']=Auth::user()->id;
        $request['updated_by']=Auth::user()->id;
        $weeks = DB::select( DB::raw("UPDATE quotes set status='Inactive'"));
        $data=Quote::create($request->all());
        return redirect()->back()->with('success','Quote created successfully');
    }

    public  function activate(Request $request,$id){
        $request['updated_by']=Auth::user()->id;
        $request['status']='Active';
        $weeks = DB::select( DB::raw("UPDATE quotes set status='Inactive'"));
        $data=Quote::find($id);
        $data->update($request->all());
        return redirect()->back()->with('success','Quote activated successfully');
    }

    public  function editquote(Request $request,$id){
        $validatedData = $request->validate([
            'verse' => 'required',
            'quote' => 'required',
        ]);
        $data=Quote::find($id);
        $request['updated_by']=Auth::user()->id;
        $data->update($request->all());
        return redirect()->back()->with('success','Quote updated successfully');
    }
}
