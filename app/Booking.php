<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected  $fillable=['user_id','service_id','status','chair'];

    public  function service(){
        return $this->belongsTo(Service::class);
    }

    public  function user(){
        return $this->belongsTo(User::class);
    }
}
