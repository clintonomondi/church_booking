<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{
    protected  $fillable=['photo','service_id'];

    public  function service(){
        return $this->belongsTo(Service::class);
    }
}
