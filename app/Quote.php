<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    protected  $fillable=['quote','verse','created_by','updated_by','status'];

}
