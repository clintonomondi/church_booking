if($('select').length > 0) {
    $('select').select2()
}
$( document ).ready(function() {
    getdata();

});
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result)

        };

        reader.readAsDataURL(input.files[0]);
    }
}
function refreshPage(){
    window.location.reload();
}
function book(service_id,id){
        event.preventDefault();
        $('#'+id).attr('disabled', true);
        $('#'+id).text('Please wait...');
        var data;
        data = new FormData();
        data.append('service_id',service_id);
        $.ajax({
            url:'/service/book',
            method:"POST",
            data: data,
            contentType:false,
            processData:false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success:function(data) {
                if(data.status) {
                    swal({
                        text: data.message,
                        icon: "success",
                    });

                    $('#'+id).attr('disabled', false);
                    $('#'+id).text('Book now!');
                    $(':input', '#submitKasae')
                        .not(':button,:submit,:hidden')
                        .val('')
                        .prop('checked', false)
                        .prop('selected', false);

                    setTimeout(function(){
                             refreshPage()
                    },3000);
                }else{
                    swal({
                        text: data.message,
                        icon: "error",
                    });
                    $('#'+id).attr('disabled', false);
                    $('#'+id).text('Book now!');
                }
            },
            error:function(data) {
                swal({
                    text: 'Invalid data or system error',
                    icon: "error",
                });
                $('#'+id).attr('disabled', false);
                $('#'+id).text('Book now!');
            }

        });
}

function cancel(book_id,id){
    event.preventDefault();
    $('#'+id).attr('disabled', true);
    $('#'+id).text('wait...');
    var data;
    data = new FormData();
    data.append('id',book_id);
    $.ajax({
        url:'/service/book/cancel',
        method:"POST",
        data: data,
        contentType:false,
        processData:false,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(data) {
            if(data.status) {
                swal({
                    text: data.message,
                    icon: "success",
                });

                $('#'+id).attr('disabled', false);
                $('#'+id).text('cancel');
                $(':input', '#submitKasae')
                    .not(':button,:submit,:hidden')
                    .val('')
                    .prop('checked', false)
                    .prop('selected', false);

                setTimeout(function(){
                    refreshPage()
                },3000);
            }else{
                swal({
                    text: data.message,
                    icon: "error",
                });
                $('#'+id).attr('disabled', false);
                $('#'+id).text('cancel');
            }
        },
        error:function(data) {
            swal({
                text: 'Invalid data or system error',
                icon: "error",
            });
            $('#'+id).attr('disabled', false);
            $('#'+id).text('cancel');
        }

    });
}

function updateRole(role,id) {
    var data;
    data = new FormData();
    data.append('id', id);
    data.append('role', role);
    $.ajax({
        url: "/role/update",
        method: "POST",
        contentType: false,
        processData: false,
        catche: false,
        data: data,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            if(data.status){
                swal({
                    text: data.message,
                    icon: "success",
                });
                setTimeout(function(){
                    refreshPage()
                },3000);
            }else{
                swal({
                    text: data.message,
                    icon: "error",
                });
            }
        },
        error:function(data) {
            swal({
                text: 'System technical error',
                icon: "error",
            });
        }
    });

}

function getdata(){
    var data;
    $.ajax({
        url: "/getdata",
        method: "GET",
        contentType: false,
        processData: false,
        catche: false,
        data: data,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            if(data.status){
                loadgraphs(data)

            }else{
                swal({
                    text: data.message,
                    icon: "error",
                });
            }
        },
        error:function(data) {
            swal({
                text: 'System technical error',
                icon: "error",
            });
        }
    });
}




function loadgraphs(source) {
    var weeks = [];
    var values = [];
    $.each(source.info, function(i, item) {
        weeks.push('Week '+item.created);
        values.push(item.number);
    });
    var data = {
        labels: weeks,
        datasets: [
            {
                label: "My First dataset",
                fillColor: "rgba(151,187,205,0.2)",
                    strokeColor: "rgba(151,187,205,1)",
                    pointColor: "rgba(151,187,205,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                data: values
            },
            // {
            //     label: "My Second dataset",
            //     fillColor: "rgba(151,187,205,0.2)",
            //     strokeColor: "rgba(151,187,205,1)",
            //     pointColor: "rgba(151,187,205,1)",
            //     pointStrokeColor: "#fff",
            //     pointHighlightFill: "#fff",
            //     pointHighlightStroke: "rgba(151,187,205,1)",
            //     data: [28, 48, 40, 19, 86]
            // }
        ]
    };
    var pdata = [
        {
            value: source.gender.male,
            color: "#46BFBD",
            highlight: "#5AD3D1",
            label: "Male"
        },
        {
            value: source.gender.female,
            color:"#F7464A",
            highlight: "#FF5A5E",
            label: "Female"
        },
        {
            value: source.gender.other,
            color:"#fff",
            highlight: "#fff",
            label: "Others"
        }
    ]

    var ctxl = $("#lineChartDemo").get(0).getContext("2d");
    var lineChart = new Chart(ctxl).Line(data);

    var ctxp = $("#pieChartDemo").get(0).getContext("2d");
    var pieChart = new Chart(ctxp).Pie(pdata);
}
function submitform2() {
    $('body').loader('show');
    document.forms["reportform"].submit();
}
function submitform() {
    document.forms["reportform"].submit();
}

