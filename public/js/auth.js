

$( document ).ready(function() {
    register();

});

function register(){
    $(document).on('submit', '#register', function(event){
        event.preventDefault();
        $('#submit').attr('disabled', true);
        $('#submit').text('Please wait..');
        var data;
        data = new FormData(this);
        $.ajax({
            url:'/user/register',
            method:"POST",
            data: data,
            contentType:false,
            processData:false,
            success:function(data) {
                if(data.status) {
                    swal({
                        text: data.message,
                        icon: "success",
                    });

                    $('#submit').attr('disabled', false);
                    $('#submit').text('Submit');
                    $(':input', '#register')
                        .not(':button,:submit,:hidden')
                        .val('')
                        .prop('checked', false)
                        .prop('selected', false);

                    setTimeout(function(){
                    window.location = '/login';
                    },3000);
                }else{
                    swal({
                        text: data.message,
                        icon: "error",
                    });
                    $('#submit').attr('disabled', false);
                    $('#submit').text('Submit');
                }
            },
            error:function(data) {
                swal({
                    text: 'Invalid data or system error',
                    icon: "error",
                });
                $('#submit').attr('disabled', false);
                $('#submit').text('Submit');
            }

        });
    });
}

function loadbutton() {
    $('#btnloader').attr('disabled', true);
    $('#btnloader').text('Please wait..');
}
function loadbutton2(id) {
    $('#btnloader'+id).attr('disabled', true);
    $('#btnloader'+id).text('Please wait..');
}
function loadbutton3(id) {
    $('#'+id).attr('disabled', true);
    $('#'+id).text('Please wait..');
}
